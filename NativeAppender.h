// LegacyAppender.h

#pragma once

#include <memory>
#include "Logger.h"


namespace Logging {

		using namespace System;
		using namespace log4net;

		public ref class NativeBridgeAppender : public log4net::Appender::AppenderSkeleton
		{
		public:
			NativeBridgeAppender();
			virtual ~NativeBridgeAppender();	

		protected:
			void Append(log4net::Core::LoggingEvent^ loggingEvent) override;
			void Append(array<log4net::Core::LoggingEvent^>^ loggingEventArray) override;
		
		protected:
			log4cpp::Category& m_log;
		};
}


	


// This is the main DLL file.

#include "stdafx.h"
#include "NativeAppender.h"

namespace Logging {

	using namespace System::Runtime::InteropServices;
	using namespace System::IO;

	NativeBridgeAppender::NativeBridgeAppender() : m_log(Logging::Logger::getRootLogger())
	{
		// call Log
		m_log.debugStream() << "[NativeBridgeAppender] " << "Loaded";
	};

	NativeBridgeAppender::~NativeBridgeAppender() {

	};

	void NativeBridgeAppender::Append(log4net::Core::LoggingEvent^ loggingEvent) {

		try {
			// log level
			String^ logLevel = loggingEvent->Level->Name->ToLower();

			//// if layout available format string
			//String^ message = "";
			//if (this->Layout) {
			//	auto sw = gcnew StringWriter();
			//	this->Layout->Format(sw, loggingEvent);
			//	
			//	// convert string to ptr
			//	message = sw->ToString();
			//}
			//else
			//{
			//}

			// use message without formatting
			String^ message = loggingEvent->RenderedMessage->ToString();
			// marshal string
			System::IntPtr strPtr = Marshal::StringToHGlobalAnsi(message);

			// call Log according to log level
			if (logLevel->Equals("debug")) {
				m_log.debug("%s", (char*)strPtr.ToPointer());
			}
			else if (logLevel->Equals("info")) {
				m_log.info("%s", (char*)strPtr.ToPointer());
			}
			else if (logLevel->Equals("error")) {
				m_log.error("%s", (char*)strPtr.ToPointer());
			}
			else if (logLevel->Equals("warn")) {
				m_log.warn("%s", (char*)strPtr.ToPointer());
			}
			// Always free the unmanaged string.
			Marshal::FreeHGlobal(strPtr);
		}
		catch (Exception^ ex) {
			System::IntPtr exStr = Marshal::StringToHGlobalAnsi(ex->ToString());
			m_log.debugStream() << "[NativeBridgeAppender.Append] Error: " << (char*)exStr.ToPointer();
			Marshal::FreeHGlobal(exStr);
		}
	};

	void NativeBridgeAppender::Append(array<log4net::Core::LoggingEvent^>^ loggingEventArray) {
		for each (auto le in loggingEventArray)
		{
			Append(le);
		}
	}
}
